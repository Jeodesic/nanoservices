package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class GatheringTest {

    private Random sizeOfList = new Random();

    // A list of positive integers is terminated with a -1, and the input consists of 25 such lists
    // Return all 25 lists, but instead prepended with the length of the list.
    // ie [1, 2, 3, 4, -1] -> [4, 1, 2, 3, 4]
    @Test
    public void gathering() throws Exception {
        List<List<Integer>> input = IntStream.range(0, 25).mapToObj(i -> getShortListOfInts()).collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(input.stream().flatMap(this::getNegativeTerminatedStream).collect(Collectors.toList()), input.stream().mapToInt(List::size).sum());

        // Register some runners
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        assertEquals(input.stream().flatMap(this::getSizePrependedStream).collect(Collectors.toList()), runner.getOutput());
    }

    private List<Integer> getShortListOfInts() {
        return new Random().ints(sizeOfList.nextInt(5) + 1, 0, Integer.MAX_VALUE).boxed().collect(Collectors.toList());
    }

    private Stream<Integer> getNegativeTerminatedStream(List<Integer> inputList) {
        return Stream.concat(inputList.stream(), Stream.of(-1));
    }

    private Stream<Integer> getSizePrependedStream(List<Integer> inputList) {
        return Stream.concat(Stream.of(inputList.size()), inputList.stream());
    }
}
