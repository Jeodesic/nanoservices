package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class StatisticsTest {
    @Test
    public void meanAndVarianceTest() throws Exception {
        List<Integer> input = new Random().ints(100, 1, 25).boxed().collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(input, 2);

        runner.run();

        int average = (int) input.stream().mapToInt(i -> i).average().orElse(-1);
        int variance = (int) input.stream().mapToInt(i -> i * i).average().orElse(-1) - average * average;
        assertEquals(average, (int) runner.getOutput().get(0));
        assertEquals(variance, (int) runner.getOutput().get(1));
    }

}
