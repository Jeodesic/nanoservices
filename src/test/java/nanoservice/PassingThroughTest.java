package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class PassingThroughTest {
    @Test
    public void passthrough() throws Exception {
        List<Integer> input = new Random().ints(100).boxed().collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(input, 100);

        // Register some runners
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        assertEquals(input, runner.getOutput());
    }
}
