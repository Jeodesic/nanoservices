package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class SquaringTest {
    @Test
    public void square() throws Exception {
        List<Integer> input = IntStream.range(0, 100).boxed().collect(Collectors.toList());
        Collections.shuffle(input);
        NanoRunner runner = new NanoRunner(input, 100);

        // Register some runners
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        assertEquals(input.stream().map(i -> i * i).collect(Collectors.toList()), runner.getOutput());
    }
}
