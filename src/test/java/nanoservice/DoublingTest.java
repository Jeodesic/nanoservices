package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class DoublingTest {
    @Test
    public void doubleTest() throws Exception {
        List<Integer> input = new Random().ints(100, 0, 100).boxed().collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(input, 100);

        // Register some services
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        assertEquals(input.stream().map(i -> 2 * i).collect(Collectors.toList()), runner.getOutput());
    }
}
