package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class BinaryTest {
    // Given a positive integer, output the bits of that number in binary padded to 16 bits, with the most significant digit first
    // Eg 5001 -> 0001001110001001

    @Test
    public void convertToBinary() throws Exception {
        List<Integer> input = new Random(66).ints(100, 0, 65536).boxed().collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(input, 1600);

        String[] code = {};
        runner.registerService("node", code);

        runner.run();

        assertEquals(input.stream().flatMap(BinaryTest::toBinaryStream).collect(Collectors.toList()), runner.getOutput());
    }

    private static Stream<Integer> toBinaryStream(Integer integer) {
        return IntStream.range(0, 16).map(i -> (int) Math.pow(2, 15 - i)).mapToObj(i -> (integer % (2 * i)) / i);
    }
}
