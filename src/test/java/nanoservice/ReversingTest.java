package nanoservice;

import com.google.common.collect.Lists;
import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class ReversingTest {

    private Random sizeOfList = new Random();

    // A list of positive integers is terminated with a -1, and the input consists of 25 such lists
    // Return all 25 lists reversed (and terminated with -1)
    @Test
    public void reversing() throws Exception {
        List<List<Integer>> input = IntStream.range(0, 25).mapToObj(i -> getShortListOfInts()).collect(Collectors.toList());
        List<Integer> flatInput = input.stream().flatMap(this::getNegativeTerminatedStream).collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(flatInput, flatInput.size());

        // Register some runners
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        assertEquals(input.stream().map(Lists::reverse).flatMap(this::getNegativeTerminatedStream).collect(Collectors.toList()), runner.getOutput());
    }

    private List<Integer> getShortListOfInts() {
        return new Random().ints(sizeOfList.nextInt(5) + 1, 0, Integer.MAX_VALUE).boxed().collect(Collectors.toList());
    }

    private Stream<Integer> getNegativeTerminatedStream(List<Integer> inputList) {
        return Stream.concat(inputList.stream(), Stream.of(-1));
    }
}
