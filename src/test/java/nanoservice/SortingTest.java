package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;

public class SortingTest {
    // Given a list of numbers, sort each set of 8 into descending order.
    // eg [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16] should return [8, 7, 6, 5, 4, 3, 2, 1, 16, 15, 14, 13, 12, 11, 10, 9]

    private Random random = new Random();
    private List<List<Integer>> numberSetsToSort = IntStream.range(0, 100)
            .mapToObj(i -> IntStream.range(0, 8).map(j -> random.nextInt()).boxed().collect(toList()))
            .collect(toList());

    @Test
    public void sorting() throws Exception {
        List<Integer> input = numberSetsToSort.stream().flatMap(List::stream).collect(toList());
        NanoRunner runner = new NanoRunner(input, 800);

        // Register some runners
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        List<Integer> output = numberSetsToSort.stream().flatMap(integers -> integers.stream().sorted()).collect(toList());
        assertEquals(output, runner.getOutput());
    }
}
