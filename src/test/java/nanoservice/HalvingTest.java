package nanoservice;

import nanoservices.NanoRunner;
import org.junit.Test;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class HalvingTest {
    // Return the positive integers halved, rounding down.
    @Test
    public void half() throws Exception {
        List<Integer> input = new Random(3).ints(100, 1000, 10000).boxed().collect(Collectors.toList());
        NanoRunner runner = new NanoRunner(input, 100);

        // Register some runners
        String[] code = {};
        runner.registerService("node", code);

        runner.run();
        assertEquals(input.stream().map(i -> i/2).collect(Collectors.toList()), runner.getOutput());
    }
}
