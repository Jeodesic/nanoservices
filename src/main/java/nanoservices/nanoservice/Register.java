package nanoservices.nanoservice;

import java.util.function.BiConsumer;
import java.util.function.Function;

public enum Register {
    A(Nanoservice::setRegisterA, Nanoservice::getRegisterA),
    B(Nanoservice::setRegisterB, Nanoservice::getRegisterB);

    private BiConsumer<Nanoservice, Integer> setter;
    private Function<Nanoservice, Integer> getter;

    Register(BiConsumer<Nanoservice, Integer> setter, Function<Nanoservice, Integer> getter) {
        this.setter = setter;
        this.getter = getter;
    }

    void set(Nanoservice service, int value) {
        setter.accept(service, value);
    }

    int get(Nanoservice service) {
        return getter.apply(service);
    }
}
