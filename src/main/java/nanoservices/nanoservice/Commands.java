package nanoservices.nanoservice;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;

public enum Commands {
    JUMP(1, (service, args) -> service.setPointer(Integer.parseInt(args.get(0)))),
    JGEQ(3, (service, args) -> service.setPointerIfGreaterOrEqual(args.get(0), args.get(1), Integer.parseInt(args.get(2)))),
    JEQU(3, (service, args) -> service.setPointerIfEqual(args.get(0), args.get(1), Integer.parseInt(args.get(2)))),
    READ(2, (service, args) -> service.read(args.get(0), Register.valueOf(args.get(1)))),
    WRTE(2, (service, args) -> service.write(args.get(0), args.get(1))),
    COPY(2, (service, args) -> service.copy(args.get(0), Register.valueOf(args.get(1)))),
    ADDI(3, (service, args) -> service.add(args.get(0), args.get(1), Register.valueOf(args.get(2)))),
    SUBI(3, (service, args) -> service.subtract(args.get(0), args.get(1), Register.valueOf(args.get(2)))),
    SNZE(0, (service, args) -> { });

    private final int numberOfArgs;
    private final BiConsumer<Nanoservice, List<String>> invoke;

    public int getNumberOfArgs() {
        return numberOfArgs;
    }

    Commands(int numberOfArgs, BiConsumer<Nanoservice, List<String>> invoke) {
        this.numberOfArgs = numberOfArgs;
        this.invoke = invoke;
    }

    public void invoke(Nanoservice service, String[] args) {
        invoke.accept(service, Arrays.asList(args));
    }
}
