package nanoservices.nanoservice;

import nanoservices.NanoRunner;
import nanoservices.exception.InvalidCodeException;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Nanoservice {
    private static final String REGISTER_REGEX = "[AB]";
    public final String name;
    private int registerA = 0;
    private int registerB = 0;
    private int pointer = 0;
    private final String[] code;
    private NanoRunner runner;
    private Integer tempPointer;

    public Nanoservice(String name, String[] code) {
        this.name = name;
        this.code = verifyCode(code);
    }

    int getRegisterA() {
        return registerA;
    }

    int getRegisterB() {
        return registerB;
    }

    private String[] verifyCode(String[] code) {
        String[] trimmedCode = Arrays.stream(code).map(String::trim).toArray(String[]::new);
        List<String> codeErrors = IntStream.range(0, trimmedCode.length)
                .mapToObj(lineNumber -> getError(lineNumber, trimmedCode[lineNumber]))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        if (codeErrors.size() > 0) {
            throw new InvalidCodeException(String.format("Code for %s has errors: %s", name, codeErrors));
        } else {
            return trimmedCode;
        }
    }

    private static String getError(int lineNumber, String line) {
        try {
            Commands command = Commands.valueOf(line.substring(0, 4));
            int numberOfArgs = line.split("\\s+").length - 1;
            if (numberOfArgs != command.getNumberOfArgs()) {
                return String.format("Line %d: %s expects %d arguments but found %d", lineNumber, command.name(), command.getNumberOfArgs(), numberOfArgs);
            }
            return null;
        } catch (IllegalArgumentException e) {
            return String.format("Line %d: %s is not a valid command.", lineNumber, line.substring(0, 4));
        }
    }

    public void executeCycle() {
        executeCodeLine(code[pointer]);
        updatePointer();
    }

    private void executeCodeLine(String s) {
        String[] split = s.split("\\s+");
        Commands.valueOf(split[0]).invoke(this, Arrays.copyOfRange(split, 1, split.length));
    }

    private void updatePointer() {
        pointer = tempPointer != null ? tempPointer : (pointer + 1) % code.length;
        tempPointer = null;
    }

    public void setRunner(NanoRunner nanoRunner) {
        runner = nanoRunner;
    }

    void setPointer(int jumpTo) {
        tempPointer = jumpTo;
    }

    void read(String readFrom, Register register) {
        runner.logRead(readFrom, name, register);
    }

    void write(String value, String writeTo) {
        runner.logWrite(name, writeTo, parseAsIntOrRegisterValue(value));
    }

    void add(String inputOne, String inputTwo, Register output) {
        add(parseAsIntOrRegisterValue(inputOne), parseAsIntOrRegisterValue(inputTwo), output);
    }

    void subtract(String inputOne, String inputTwo, Register output) {
        add(parseAsIntOrRegisterValue(inputOne), -parseAsIntOrRegisterValue(inputTwo), output);
    }

    void copy(String input, Register output) {
        output.set(this, parseAsIntOrRegisterValue(input));
    }

    private int parseAsIntOrRegisterValue(String input) {
        return input.matches(REGISTER_REGEX) ? Register.valueOf(input).get(this) : Integer.parseInt(input);
    }

    private void add(int valueOne, int valueTwo, Register output) {
        output.set(this, valueOne + valueTwo);
    }

    void setRegisterA(int value) {
        registerA = value;
    }

    void setRegisterB(int value) {
        registerB = value;
    }

    public void setRegister(Integer value, Register register) {
        register.set(this, value);
    }

    void setPointerIfEqual(String inputOne, String inputTwo, int jumpTo) {
        if (parseAsIntOrRegisterValue(inputOne) == parseAsIntOrRegisterValue(inputTwo)) {
            setPointer(jumpTo);
        }
    }

    void setPointerIfGreaterOrEqual(String inputOne, String inputTwo, int jumpTo) {
        if(parseAsIntOrRegisterValue(inputOne) >= parseAsIntOrRegisterValue(inputTwo)) {
            setPointer(jumpTo);
        }
    }

    public int getCodeSize() {
        return code.length;
    }

    public void printState() {
        System.out.println(String.format("%s: Pointer at %d, A=%d, B=%d", name, pointer, registerA, registerB));
    }
}
