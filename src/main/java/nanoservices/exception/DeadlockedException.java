package nanoservices.exception;

public class DeadlockedException extends Exception {
    public DeadlockedException(String message) {
        super(message);
    }
}
