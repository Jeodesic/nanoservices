package nanoservices;

import nanoservices.exception.DeadlockedException;
import nanoservices.nanoservice.Nanoservice;
import nanoservices.nanoservice.Register;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class NanoRunner {
    private static final String IN = "in";
    private static final String OUT = "out";
    private Map<String, Nanoservice> serviceMap = new HashMap<>();
    private Iterator<Integer> input;
    private int expectedOutputSize;
    private int cycleCounter = 0;
    private List<Integer> output = Collections.synchronizedList(new ArrayList<>());
    private Map<MessageHeader, MessageValue> pendingMessages = new HashMap<>();
    private Collection<Nanoservice> activeServices = new HashSet<>();

    public NanoRunner(List<Integer> input, int expectedOutputSize) {
        this.input = input.iterator();
        this.expectedOutputSize = expectedOutputSize;
    }

    public void run() throws DeadlockedException {
        while (notFinished() && !isDeadlocked()) {
            runSingleCycle();
        }
        if (isDeadlocked()) {
            printDeadlockDebugInformation();
            throw new DeadlockedException(String.format("System deadlocked after %d cycles", cycleCounter));
        }
        System.out.println(String.format("Run finished in %d cycles, using %d services and %d lines of code.", cycleCounter, serviceMap.size(), getTotalCodeSize()));
    }

    public void runWithDebug() throws DeadlockedException {
        while (notFinished() && !isDeadlocked()) {
            runSingleCycle();
            serviceMap.keySet().stream().sorted().map(serviceMap::get).forEach(Nanoservice::printState);
            System.out.println("---------");
        }
        if (isDeadlocked()) {
            printDeadlockDebugInformation();
            throw new DeadlockedException(String.format("System deadlocked after %d cycles", cycleCounter));
        }
        System.out.println(String.format("Run finished in %d cycles, using %d services and %d lines of code.", cycleCounter, serviceMap.size(), getTotalCodeSize()));
    }

    private void printDeadlockDebugInformation() {
        System.out.println("Deadlocked");
        pendingMessages.forEach((key, value) -> System.out.println(value.register == null
                ? key.fromId + ": Waiting for READ from " + key.toId
                : key.toId + ": Waiting for WRITE from " + key.fromId));
    }

    private int getTotalCodeSize() {
        return serviceMap.values().stream().mapToInt(Nanoservice::getCodeSize).sum();
    }

    private boolean isDeadlocked() {
        return activeServices.size() == 0;
    }

    private boolean notFinished() {
        return output.size() < expectedOutputSize;
    }

    public void logRead(String fromId, String toId, Register register) {
        pendingMessages.computeIfAbsent(new MessageHeader(fromId, toId), (header) -> new MessageValue()).register = register;
    }

    public void logWrite(String fromId, String toId, Integer writeValue) {
        pendingMessages.computeIfAbsent(new MessageHeader(fromId, toId), (header) -> new MessageValue()).value = writeValue;
    }

    private void runSingleCycle() {
        cycleCounter += 1;
        activeServices.forEach(Nanoservice::executeCycle);
        handleMessages();
        updateActiveServices();
    }

    private void updateActiveServices() {
        Set<String> waitingServices = pendingMessages.entrySet().stream()
                .map(NanoRunner::toWaitingService)
                .collect(Collectors.toSet());
        activeServices = serviceMap.entrySet().stream()
                .filter(entry -> !waitingServices.contains(entry.getKey()))
                .map(Map.Entry::getValue)
                .collect(Collectors.toSet());
    }

    private static String toWaitingService(Map.Entry<MessageHeader, MessageValue> entry) {
        return entry.getValue().value == null ? entry.getKey().toId : entry.getKey().fromId;
    }

    private void handleMessages() {
        List<MessageHeader> completedMessages = pendingMessages.entrySet().stream()
                .filter(this::isCompletableMessage)
                .map(this::completeMessage)
                .collect(Collectors.toList());
        completedMessages.forEach(pendingMessages::remove);
    }

    private MessageHeader completeMessage(Map.Entry<MessageHeader, MessageValue> entry) {
        if (entry.getKey().fromId.equals(IN)) {
            Integer value = input.hasNext() ? input.next() : -1;
            serviceMap.get(entry.getKey().toId).setRegister(value, entry.getValue().register);
        } else if (entry.getKey().toId.equals(OUT)) {
            output.add(entry.getValue().value);
        } else {
            serviceMap.get(entry.getKey().toId).setRegister(entry.getValue().value, entry.getValue().register);
        }
        return entry.getKey();
    }

    private boolean isCompletableMessage(Map.Entry<MessageHeader, MessageValue> entry) {
        return (entry.getValue().register != null && entry.getValue().value != null)
                || entry.getKey().fromId.equals(IN)
                || entry.getKey().toId.equals(OUT);
    }

    public List<Integer> getOutput() {
        return output;
    }

    public void registerService(String name, String[] code) {
        Nanoservice service = new Nanoservice(name, code);
        service.setRunner(this);
        serviceMap.put(service.name.toLowerCase(), service);
        activeServices.add(service);
    }

    public Collection<String> getServiceNames() {
        return serviceMap.keySet();
    }

    private static class MessageHeader {
        private final String fromId;
        private final String toId;

        private MessageHeader(String fromId, String toId) {
            this.fromId = fromId.toLowerCase();
            this.toId = toId.toLowerCase();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            MessageHeader that = (MessageHeader) o;
            return Objects.equals(fromId, that.fromId) &&
                    Objects.equals(toId, that.toId);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fromId, toId);
        }

        @Override
        public String toString() {
            return "MessageHeader{" +
                    "fromId='" + fromId + '\'' +
                    ", toId='" + toId + '\'' +
                    '}';
        }
    }

    private static class MessageValue {
        private Register register;
        private Integer value;

        @Override
        public String toString() {
            return "MessageValue{" +
                    "register=" + register +
                    ", value=" + value +
                    '}';
        }
    }
}
